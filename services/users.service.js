"use strict";

const { MoleculerClientError } = require("moleculer").Errors;

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const DbService = require("../mixins/db.mixin");

module.exports = {
  name: "users",
  mixins: [DbService("users")],

  /**
   * Default settings
   */
  settings: {
    /** REST Basepath */
    rest: "/",

    /** Secret for JWT */
    JWT_SECRET: process.env.JWT_SECRET || "nebrija-2021",

    /** Public fields */
    fields: ["_id", "username", "email", "image"],

    /** Validator schema for entity */
    entityValidator: {
      username: { type: "string", min: 2 },
      password: { type: "string", min: 6 },
      email: { type: "email" },
      image: { type: "string", optional: true },
    },
  },

  /**
   * Actions
   */
  actions: {
    /**
     * Register a new user
     *
     * @actions
     * @param {Object} user - User
     *
     * @returns {Object} Created user & token
     */
    create: {
      rest: "POST /users",
      params: {
        user: { type: "object" },
      },
      async handler(ctx) {
        let user = ctx.params.user;
        await this.validateEntity(user);

        if (user.username) {
          const found = await this.adapter.findOne({
            username: user.username,
          });

          if (found)
            throw new MoleculerClientError("Username is exist!", 422, "", [
              { field: "username", message: "is exist" },
            ]);
        }

        if (user.email) {
          const found = await this.adapter.findOne({ email: user.email });

          if (found)
            throw new MoleculerClientError("Email is exist!", 422, "", [
              { field: "email", message: "is exist" },
            ]);
        }

        user.password = bcrypt.hashSync(user.password);
        user.createdAt = new Date();

        const doc = await this.adapter.insert(user);
        const json = await this.transformUser(doc, true, ctx.meta.token);
        await this.entityChanged("created", json, ctx);

        return json;
      },
    },

    /**
     * Login with username & password
     *
     * @actions
     * @param {Object} user - User credentials
     *
     * @returns {Object} Logged in user with token
     */
    login: {
      rest: "POST /users/login",
      params: {
        user: {
          type: "object",
          props: {
            email: { type: "email" },
            password: { type: "string", min: 1 },
          },
        },
      },
      async handler(ctx) {
        const { email, password } = ctx.params.user;

        // Find user on DB
        const user = await this.adapter.findOne({ email });
        if (!user) {
          throw new MoleculerClientError(
            "Email or password is invalid!",
            422,
            "",
            [{ field: "email", message: "is not found" }]
          );
        }

        // Check user password
        const res = await bcrypt.compare(password, user.password);
        if (!res)
          throw new MoleculerClientError("Wrong password!", 422, "", [
            { field: "password", message: "incorrect" },
          ]);

        // Remove password and all protected fields from response
        const doc = await this.transformDocuments(ctx, {}, user);
        const transformed = await this.transformUser(doc, true, ctx.meta.token);

        return transformed;
      },
    },

    /**
     * Get user by JWT token (for API GW authentication)
     *
     * @actions
     * @param {String} token - JWT token
     *
     * @returns {Object} Resolved user
     */
    resolveToken: {
      params: {
        token: "string",
      },
      async handler(ctx) {
        const decoded = await new this.Promise((resolve, reject) => {
          jwt.verify(
            ctx.params.token,
            this.settings.JWT_SECRET,
            (err, decoded) => {
              if (err) return reject(err);

              resolve(decoded);
            }
          );
        });

        if (decoded.id) return this.getById(decoded.id);
      },
    },

    /**
     * Get current user entity.
     * Auth is required!
     *
     * @actions
     *
     * @returns {Object} User entity
     */
    me: {
      auth: "required",
      rest: "GET /user",
      async handler(ctx) {
        const user = await this.getById(ctx.meta.user._id);
        if (!user) throw new MoleculerClientError("User not found!", 400);

        const doc = await this.transformDocuments(ctx, {}, user);
        return await this.transformUser(doc, true, ctx.meta.token);
      },
    },

    /**
     * Update current user entity.
     * Auth is required!
     *
     * @actions
     *
     * @param {Object} user - Modified fields
     * @returns {Object} User entity
     */
    updateMyself: {
      auth: "required",
      rest: "PUT /user",
      params: {
        user: {
          type: "object",
          props: {
            username: {
              type: "string",
              min: 2,
              optional: true,
              pattern: /^[a-zA-Z0-9]+$/,
            },
            password: { type: "string", min: 6, optional: true },
            email: { type: "email", optional: true },
            image: { type: "string", optional: true },
          },
        },
      },
      async handler(ctx) {
        const newData = ctx.params.user;

        if (newData.username) {
          const found = await this.adapter.findOne({
            username: newData.username,
          });
          if (found && found._id.toString() !== ctx.meta.user._id.toString())
            throw new MoleculerClientError("Username is exist!", 422, "", [
              { field: "username", message: "is exist" },
            ]);
        }

        if (newData.email) {
          const found = await this.adapter.findOne({ email: newData.email });
          if (found && found._id.toString() !== ctx.meta.user._id.toString())
            throw new MoleculerClientError("Email is exist!", 422, "", [
              { field: "email", message: "is exist" },
            ]);
        }

        newData.updatedAt = new Date();

        const update = {
          $set: newData,
        };

        const doc = await this.adapter.updateById(ctx.meta.user._id, update);

        const user = await this.transformDocuments(ctx, {}, doc);
        const json = await this.transformUser(user, true, ctx.meta.token);
        await this.entityChanged("updated", json, ctx);
        return json;
      },
    },

    list: {
      rest: "GET /users",
    },

    get: {
      rest: "GET /users/:id",
    },

    update: {
      rest: "PUT /users/:id",
    },

    remove: {
      rest: "DELETE /users/:id",
    },

    /**
     * Get a user profile.
     *
     * @actions
     *
     * @param {String} username - Username
     * @returns {Object} User entity
     */
    profile: {
      rest: "GET /profiles/:username",
      params: {
        username: { type: "string" },
      },
      async handler(ctx) {
        const user = await this.adapter.findOne({
          username: ctx.params.username,
        });
        if (!user) throw new MoleculerClientError("User not found!", 404);

        const doc = await this.transformDocuments(ctx, {}, user);
        return doc;
      },
    },
  },

  /**
   * Methods
   */
  methods: {
    /**
     * Generate a JWT token from user entity
     *
     * @param {Object} user
     */
    generateJWT(user) {
      const today = new Date();
      const exp = new Date(today);
      exp.setDate(today.getDate() + 60);

      return jwt.sign(
        {
          id: user._id,
          username: user.username,
          exp: Math.floor(exp.getTime() / 1000),
        },
        this.settings.JWT_SECRET
      );
    },

    /**
     * Transform user. Generate JWT token if neccessary.
     *
     * @param {Object} user
     * @param {Boolean} withToken
     * @param {String} token
     */
    transformUser(user, withToken, token) {
      if (user) {
        if (withToken) user.token = token || this.generateJWT(user);
      }

      return { user };
    },
  },
};
