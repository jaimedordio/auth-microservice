"use strict";

const _ = require("lodash");
const ApiGateway = require("moleculer-web");
const { UnAuthorizedError } = ApiGateway.Errors;

module.exports = {
  name: "api",
  mixins: [ApiGateway],

  settings: {
    port: process.env.PORT || 3000,

    routes: [
      {
        path: "/",

        authorization: true,
        autoAliases: true,

        // Set CORS headers
        cors: true,

        // Parse body content
        bodyParsers: {
          json: {
            strict: false,
          },
          urlencoded: {
            extended: false,
          },
        },
      },
    ],

    assets: {
      folder: "./public",
    },

    logRequestParams: "info",
    // logResponseData: "info",

    onError(req, res, err) {
      // Return with the error as JSON object
      res.setHeader("Content-type", "application/json; charset=utf-8");
      res.writeHead(err.code || 500);

      if (err.code == 422) {
        let allErrors = {};

        err.data.forEach((error) => {
          let field = error.field.split(".").pop();
          allErrors[field] = error.message;
        });

        res.end(JSON.stringify({ errors: allErrors }));
      } else {
        const errObj = _.pick(err, ["name", "message", "code", "type", "data"]);
        res.end(JSON.stringify(errObj, null, 2));
      }

      this.logResponse(req, res, err ? err.ctx : null);
    },
  },

  actions: {
    /**
     * Test action
     *
     * @returns {Object} Test string
     */
    test: {
      auth: "required",
      rest: "GET /testing",
      async handler(ctx) {
        return "test";
      },
    },
  },

  methods: {
    /**
     * Authorize the request
     *
     * @param {Context} ctx
     * @param {Object} route
     * @param {IncomingRequest} req
     * @returns {Promise}
     */
    async authorize(ctx, route, req) {
      let token;

      if (req.headers.authorization) {
        let type = req.headers.authorization.split(" ")[0];

        if (type === "Token" || type === "Bearer")
          token = req.headers.authorization.split(" ")[1];
      }

      let user;

      if (token) {
        // Verify JWT token
        try {
          user = await ctx.call("users.resolveToken", { token });

          if (user) {
            this.logger.info("Authenticated via JWT: ", user.username);

            // Reduce user fields (it will be transferred to other nodes)
            ctx.meta.user = _.pick(user, ["_id", "username", "email", "image"]);
            ctx.meta.token = token;
            ctx.meta.userID = user._id;
          }
        } catch (err) {
          this.logger.info("User not authenticated");
        }
      }

      if (req.$action.auth == "required" && !user)
        throw new UnAuthorizedError();
    },
  },
};
